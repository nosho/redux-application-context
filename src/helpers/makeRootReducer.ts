import {ApplicationContext} from "application-context";
import {combineReducers, Reducer as _Reducer, ReducersMapObject} from "redux";
import {StateDefinition} from "redux-utils";
import {Reducer} from "../classes/Reducer";

export function makeRootReducer<StateTree>(stateDefinition: StateDefinition<StateTree>, applicationContext: ApplicationContext<any>): _Reducer<StateTree> {
    const dst: ReducersMapObject = {};

    function makeReducer(stateDefinition, dst) {
        Object.keys(stateDefinition)
            .forEach(key => {
                if (typeof stateDefinition[key] === "function") {
                    dst[key] = stateDefinition[key].prototype && stateDefinition[key].prototype instanceof Reducer
                        ? (stateDefinition[key] as typeof Reducer).createReducerWithApplicationContext(applicationContext)
                        : stateDefinition[key];
                } else if (typeof stateDefinition[key] === "object") {
                    dst[key] = combineReducers(makeReducer(stateDefinition[key], dst[key] = dst[key] || {}));
                }
            });

        return dst;
    }

    return combineReducers<StateTree>(makeReducer(stateDefinition, dst));
}

export default makeRootReducer;