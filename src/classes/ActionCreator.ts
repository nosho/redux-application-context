import {ApplicationContext, ApplicationContextPropertyName, injectDependencies} from "application-context";
import {ActionCreatorsMapObject, bindActionCreators, Dispatch} from "redux";
import {ActionCreator as _ActionCreator} from "redux-utils";

export class ActionCreator<ActionCreators> extends _ActionCreator<ActionCreators> {

    public static bindToDispatch<TActionCreator extends _ActionCreator<any>>(dispatch: Dispatch<any>): TActionCreator {
        throw new Error(`Use bindToDispatchAndApplicationContext instead of bindToDispatch`);
    }

    public static bindToDispatchAndApplicationContext<TActionCreator extends _ActionCreator<any>>(dispatch: Dispatch<any>, applicationContextFactory: () => ApplicationContext<any>): TActionCreator {
        return bindActionCreators<ActionCreatorsMapObject, TActionCreator>(
            new (this as any)(dispatch, applicationContextFactory).serialize(),
            dispatch
        );
    }

    constructor(dispatch: Dispatch<any>, applicationContextFactory: () => ApplicationContext<any>) {
        super(dispatch);
        Object.defineProperty(this, ApplicationContextPropertyName, {
            enumerable: true,
            get: () => applicationContextFactory()
        });
    }

    public serialize(): ActionCreatorsMapObject {
        injectDependencies(this, () => this[ApplicationContextPropertyName]);
        return super.serialize();
    }

}

export default ActionCreator;