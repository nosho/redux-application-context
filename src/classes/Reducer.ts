import {ApplicationContext, injectDependencies} from "application-context";
import {Reducer as _Reducer} from "redux";
import {Reducer as BaseReducer} from "redux-utils";
import "reflect-metadata";

export abstract class Reducer<State> extends BaseReducer<State> {

    [name: string]: any;

    public static createReducer<State>(): _Reducer<State> {
        throw new Error(`Use createReducerWithApplicationContext instead of createReducer.`);
    }

    public static createReducerWithApplicationContext<State>(applicationContext: ApplicationContext<any>): _Reducer<State> {
        return new (this as any)(applicationContext).reduce;
    }

    constructor(applicationContext: ApplicationContext<any>) {
        super();
        injectDependencies(this, () => applicationContext)
    }

    protected abstract getInitialState(): State;

}

export default Reducer;