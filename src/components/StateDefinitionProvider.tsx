import {ApplicationContextPropTypes} from "application-context";
import * as React from "react";
import {applyMiddleware, createStore} from "redux";
import {
    ActionCreator,
    StateDefinitionProvider as _StateDefinitionProvider,
    StateDefinitionProviderProps as _StateDefinitionProviderProps,
    StateDefinitionProviderState as _StateDefinitionProviderState
} from "redux-utils";
import {makeRootReducer} from "../helpers/makeRootReducer";

export interface StateDefinitionProviderProps<StateTree> extends _StateDefinitionProviderProps<StateTree> {

}

export interface StateDefinitionProviderState<StateTree> extends _StateDefinitionProviderState<StateTree> {

}

export class StateDefinitionProvider<Props extends StateDefinitionProviderProps<StateTree>, State extends StateDefinitionProviderState<StateTree>, StateTree> extends _StateDefinitionProvider<Props, State, StateTree> {

    //noinspection JSUnusedGlobalSymbols
    public static contextTypes: React.ValidationMap<any> = ApplicationContextPropTypes;

    protected componentWillMount(): void {
        this.setState({
            store: createStore<StateTree>(
                makeRootReducer<StateTree>(this.props.stateDefinition, this.context.applicationContext),
                this.props.enhancer || require("redux-devtools-extension").composeWithDevTools(
                    applyMiddleware(
                        ...ActionCreator.middleware,
                        ...(this.props.middleware || [])
                    )
                )
            )
        });
    }

}

export default StateDefinitionProvider;