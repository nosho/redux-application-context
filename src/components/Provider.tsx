import {
    ApplicationContextProperties,
    ApplicationContextProvider,
    ApplicationContextProviderProps
} from "application-context";
import * as React from "react";
import {StateDefinitionProviderProps} from "redux-utils";
import {StateDefinitionProvider} from "./StateDefinitionProvider";

export type ProviderProps<Properties extends ApplicationContextProperties, StateTree> =
    ApplicationContextProviderProps<Properties>
    & StateDefinitionProviderProps<StateTree>

export class Provider<Properties extends ApplicationContextProperties, StateTree> extends React.Component<ProviderProps<any, any>, void> {

    public render(): JSX.Element {
        return (
            <ApplicationContextProvider profiles={this.props.profiles} properties={this.props.properties}>
                <StateDefinitionProvider stateDefinition={this.props.stateDefinition}
                                         enhancer={this.props.enhancer}
                                         middleware={this.props.middleware}>
                    {this.props.children}
                </StateDefinitionProvider>
            </ApplicationContextProvider>
        );
    }

}

//noinspection JSUnusedGlobalSymbols
export default Provider;