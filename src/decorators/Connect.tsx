import {
    ApplicationContext,
    ApplicationContextPropTypes,
    injectDependencies,
    RemoveChangeListener
} from "application-context";
import * as React from "react";
import {connect, MapDispatchToProps, MapStateToProps} from "react-redux";
import {Dispatch} from "redux";
import {ActionCreator} from "../classes";

export interface ActionCreatorMap {
    [name: string]: ActionCreatorMap | typeof ActionCreator;
}

export function Connect<TStateProps, TDispatchProps, TOwnProps>(mapStateToProps?: MapStateToProps<TStateProps, TOwnProps>,
                                                                actionCreatorMap?: ActionCreatorMap): ClassDecorator {
    return (target: React.ComponentClass<any>): React.ComponentClass<any> => {
        const componentDidMount = target.prototype.componentDidMount;
        const componentWillUnmount = target.prototype.componentWillUnmount;
        let removeListener: RemoveChangeListener;
        let applicationContext: ApplicationContext<any>;

        target.contextTypes = Object.assign(target.contextTypes || {}, ApplicationContextPropTypes);

        const ConnectedComponent = connect<TStateProps, TDispatchProps, TOwnProps>(
            mapStateToProps,
            actionCreatorMap && transformActionCreatorMapToDispatchToPropsMapWithApplicationContext(actionCreatorMap, () => applicationContext)
        )(target);

        class WrappedComponent extends target {

            constructor(props, context) {
                super(props, context);
                applicationContext = context.applicationContext || applicationContext;
                injectDependencies(this, () => applicationContext);
            }

            protected componentDidMount() {
                // @todo find a better way of re-rendering than calling forceUpdate()
                removeListener = applicationContext.addChangeListener(() => this.forceUpdate())
                if (componentDidMount) componentDidMount();
            }

            protected componentWillUnmount() {
                removeListener();
                if (componentWillUnmount) componentWillUnmount();
            }

            public render() {
                return <ConnectedComponent {...this.props}/>;
            }

        }

        Object.assign(WrappedComponent, target);

        Object.defineProperty(WrappedComponent, "name", {value: target.name});

        (WrappedComponent as any).propTypes = target.propTypes;
        (WrappedComponent as any).contextTypes = Object.assign(target.contextTypes || {}, ApplicationContextPropTypes);
        (WrappedComponent as any).childContextTypes = target.childContextTypes;
        (WrappedComponent as any).defaultProps = target.defaultProps;
        (WrappedComponent as any).displayName = target.displayName;
        (WrappedComponent as any).WrappedComponent = target;

        return WrappedComponent;
    };
}

function transformActionCreatorMapToDispatchToPropsMapWithApplicationContext(actionCreatorMap: ActionCreatorMap, applicationContextFactory: () => ApplicationContext<any>): MapDispatchToProps<any, any> {
    function transform(dispatch: Dispatch<any>, _actionCreatorMap: ActionCreatorMap = actionCreatorMap, props: any = {}) {
        Object.keys(_actionCreatorMap)
            .forEach(key => {
                const value = _actionCreatorMap[key];

                if ((value as typeof ActionCreator).prototype instanceof ActionCreator) {
                    props[key] = (value as typeof ActionCreator).bindToDispatchAndApplicationContext(dispatch, applicationContextFactory);
                } else if (typeof value === "object") {
                    props[key] = transform(dispatch, value, props[key]);
                }
            });

        return props;
    }

    return (dispatch: Dispatch<any>) => transform(dispatch);
}

export default Connect;
